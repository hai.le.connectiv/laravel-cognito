<?php
namespace App\Auth;

use App\Cognito\CognitoClient;
use App\Exceptions\InvalidUserModelException;
use App\Exceptions\NoLocalUserException;
use Aws\Result;
use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Session\Session;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\Request;


class CognitoGuard extends SessionGuard implements StatefulGuard
{
    /**
     * @var CognitoClient
     */
    protected $client;

    /**
     * CognitoGuard constructor.
     * @param string $name
     * @param CognitoClient $client
     * @param UserProvider $provider
     * @param Session $session
     * @param null|Request $request

     */
    public function __construct(
        string $name,
        CognitoClient $client,
        UserProvider $provider,
        Session $session,
        ?Request $request = null
    ) {
        $this->client = $client;
        parent::__construct($name, $provider, $session, $request);
    }

    /**
     * @param mixed $user
     * @param array $credentials
     * @return bool
     * @throws InvalidUserModelException
     */
    protected function hasValidCredentials($user, $credentials)
    {
        /** @var Result $response */
        $result = $this->client->authenticate($credentials['email'], $credentials['password']);

        if ($result && $user instanceof Authenticatable) {
            return true;
        }

        return false;
    }

    // 中略
    /**
     * register
     * ユーザーを新規登録
     */
    public function register($email, $pass, $attributes = [])
    {
        $username = $this->client->register($email, $pass, $attributes);
        return $username;
    }

    /**
     * getCognitoUser
     * メールアドレスからCognitoのユーザー名を取得
     */
    public function getCognitoUser($email)
    {
        return $this->client->getUser($email) ? $this->client->getUser($email) : ['error' => 'false'];
    }

// 中略

    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param  array  $credentials
     * @param  bool   $remember
     * @throws
     * @return bool
     */
    public function attempt(array $credentials = [], $remember = false)
    {
        $this->fireAttemptEvent($credentials, $remember);

        $this->lastAttempted = $user = $this->provider->retrieveByCredentials($credentials);

        if ($this->hasValidCredentials($user, $credentials)) {
            $this->login($user, $remember);
            return true;
        }

        $this->fireFailedEvent($user, $credentials);

        return false;
    }

    /**
     * confirmSignUp
     *
     * @param $username
     * @param $code
     * @return bool
     */
    public function confirmSignUp($username, $code)
    {
        return $this->client->confirmSignUp($username, $code);
    }

    /**
     * resendConfirmationCode
     *
     * @param mixed $user
     * @param array $credentials
     * @return bool
     */
    public function resendConfirmationCode($username)
    {
        return $this->client->resendConfirmationCode($username);
    }

    /**
     * sendResetLink
     *
     * @param mixed $user
     * @param array $credentials
     * @return bool
     */
    public function sendResetLink($username)
    {
        return $this->client->sendResetLink($username);
    }

    /**
     * confirmForgotPassword
     *
     * @param mixed $user
     * @param array $credentials
     * @return bool
     */

    public function confirmForgotPassword($username, $password, $code)
    {
        return $this->client->confirmForgotPassword($username, $password, $code);
    }
}
