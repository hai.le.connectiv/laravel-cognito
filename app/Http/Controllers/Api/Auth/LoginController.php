<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    /**
     * Login user
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        try {
            $tokenCognito = $this->attemptLogin($request);
            if ($tokenCognito) {
                return $this->sendLoginResponseWithToken($request, $tokenCognito);
            }
        } catch (CognitoIdentityProviderException $c) {
            return $this->sendFailedCognitoResponse($c);
        } catch (\Exception $e) {
            return $this->sendFailedLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function sendLoginResponseWithToken($request, $tokenCognito)
    {
//        dd($tokenCognito['AuthenticationResult']);
        // dd($data['AuthenticationResult']['AccessToken']);
        $credentials = $request->only(['email', 'password']);
        $token = JWTAuth::attempt($credentials, $request->filled('remember'));
        if (!$token) {
            return response()->json(['無効なメールアドレスまたはパスワードです。'], 422);
        }

        return response()->json([
            'access_token' => $token,
//            'token_cognito' => $tokenCognito['AuthenticationResult'],
            'email_verified' => Auth::user()->email_verified,
            'token_type' => 'bearer',
            'expires_in' => config('JWT_TTL', 86400)
        ]);
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json([
            $this->username() => [trans('auth.failed')]
        ], 422);
//        return [$this->username() => [trans('auth.failed')]];
    }

    /**
     * send Failed Cognito Response
     *
     * @param CognitoIdentityProviderException $exception
     * @return void
     * @throws ValidationException
     */
    private function sendFailedCognitoResponse(CognitoIdentityProviderException $exception)
    {
        return response()->json([
            $this->username() => [trans('auth.failed')]
        ], 422);
//        return [$this->username() => [trans('auth.failed')]];
    }

}
