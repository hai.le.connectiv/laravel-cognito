<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\ChangePassword;
use App\User;
use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Illuminate\Foundation\Auth\ResetsPasswords;
// 追加
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Exceptions\HttpResponseException;
use Auth;
use App\Http\Requests\EditPasswordRequest;

class PasswordResetController extends Controller
{

    use ResetsPasswords;

    /**
     * Where to redirect.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * $authManager
     *
     * @var object
     */
    protected $AuthManager;

    /**
     * Create a new controller instance.
     *
     * @param AuthManager $AuthManager
     */
    public function __construct(AuthManager $AuthManager)
    {
        // $this->middleware('guest');

        // CognitoのGuardを読み込む
        $this->AuthManager = $AuthManager;
    }

    /**
     * Function: sendPhoneNumber
     * Decription: Enter email to receive a password reset code
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResetLink(Request $request)
    {
        $user =  User::where('email', $request->email)->first();

        if (!$user) {
            throw new HttpResponseException(response()->json([
                'errors' => ['email' => 'The email address is not existed.'],
            ], 422));
        };

        $this->AuthManager->sendResetLink($request->email);
        return response()->json([
            'status' => 200,
            'message'=> 'send code success'
        ]);
    }

    /**
     * Function: confirmForgetPassword
     * Decription: confirm password reset with username as email
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function confirmForgetPassword(Request $request)
    {
        $this->AuthManager->confirmForgotPassword($request->email, $request->password, $request->code);
        return response()->json([
            'status' => 200,
            'message'=> 'reset password successful'
        ]);
    }
}
