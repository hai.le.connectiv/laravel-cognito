<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\User\CreateUserRequest;
use App\User;
use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Hash;　// 削除
use Illuminate\Foundation\Auth\RegistersUsers;
// 追加
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Tymon\JWTAuth\Facades\JWTAuth;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/';

    private $AuthManager; // 追加

    public function __construct(AuthManager $AuthManager)
    {
        // CognitoのGuardを読み込む
        $this->AuthManager = $AuthManager;
    }

    public function register(CreateUserRequest $request)
    {
        $data = $request->all();

        // Cognito側の新規登録
        $username = $this->AuthManager->register(
            $data['email'],
            $data['password'],
            [
                'email' => $data['email'],
            ]
        );

        // Laravel側の新規登録
        $user = $this->create($data, $username);
        event(new Registered($user));
        return response()->json(['message' => 'User registration successful.']);
    }

    protected function create(array $data, $username)
    {
        return User::create([
            'cognito_username' => $username,
            'email'            => $data['email'],
            'email_verified'   => 'registered',
        ]);
    }


    /**
     * Function: confirmRegistration
     * Decription: Verify account registration.
     * @param Request $request
     * @return \Illuminate\Http\JsonRespons
     */
    public function confirmRegistration(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $this->AuthManager->confirmSignUp($request->email, $request->code);

        $user->update([
            'email_verified' => "confirmed"
        ]);
        $token = JWTAuth::fromUser($user);

        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => config('JWT_TTL', 86400)
        ]);
    }

}

