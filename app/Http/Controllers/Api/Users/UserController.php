<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Resources\UserResource;
use App\User;
use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Auth;

class UserController extends Controller
{

    private $AuthManager; // 追加

    public function __construct(AuthManager $AuthManager)
    {
        // CognitoのGuardを読み込む
        $this->AuthManager = $AuthManager;
    }

    public function getUserInfo(Request  $request)
    {
        $userCurrent = JWTAuth::toUser($request->header('Authorization'));

        return response()->json(['data' => new UserResource($userCurrent)], 200);
    }


    /**
     *
     * change email
     *
     * @param $data
     */
    public function changeEmail($data)
    {
        try {
            $this->AuthManager->register(
                $data['email_new'],
                $data['password']
            );
        } catch (CognitoIdentityProviderException $e) {
            throw $e;
        }
    }

    public function updateEmailVerification(Request $request)
    {
        $credentials = [
            'email' =>Auth::user()->email,
            'password' => $request->password
        ];

        JWTAuth::attempt($credentials);

        $emailVerification = DB::table('users')->where('email_verification', $request->email_new)->first();
        $data = $request->all();

        if (DB::table('users')->where('email', $request->email_new)->first()) {
            throw new HttpResponseException(response()->json([
                'errors' => ['email' => 'メールアドレスが存在します。'],
            ], 422));
        }
        if (!empty($data['email_new']) && $emailVerification) {
            $this->AuthManager->resendConfirmationCode($data['email_new']);
            return response()->json(['message' => 'Email changed successfully.']);
        } elseif (!empty($data['email_new'])) {
            $this->changeEmail($data);
            DB::table('users')->where('email', $request->email_old)->update([
                'email_verification' => $request->email_new
            ]);
            return response()->json(['message' => 'Email changed successfully.'], 200);
        }

        return response()->json(['message' => "False"]);
    }


    /**
     * Confirm phone_verification update
     *
     * @param Request $request
     * @return Json
     */
    public function confirmUpdateEmail(Request $request)
    {
        $user = User::where('email_verification', $request->email_verification)->first();

        $this->AuthManager->confirmSignUp($request->email_verification, $request->code);
        $user->update([
            'email_verified' => "confirmed",
            'email' =>$request->email_verification
        ]);
        $token = JWTAuth::fromUser($user);
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => config('JWT_TTL', 86400)
        ]);
    }
}
