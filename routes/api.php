<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', 'Api\Auth\RegisterController@register');
Route::post('/user-register-confirmed', 'Api\Auth\RegisterController@confirmRegistration');
Route::post('/login', 'Api\Auth\LoginController@login');
Route::post('/logout', 'Api\AuthController@logout');


Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('user-info', 'Api\Users\UserController@getUserInfo');
    Route::post('/update-email-verification', 'Api\Users\UserController@updateEmailVerification');
    Route::post('/confirm-update-email-verification', 'Api\Users\UserController@confirmUpdateEmail');
    Route::post('/user-forgot-password', 'Api\Auth\PasswordResetController@sendResetLink');
    Route::post('/reset-password', 'Api\Auth\PasswordResetController@confirmForgetPassword');
});
