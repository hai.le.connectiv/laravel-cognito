<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();

        \App\User::insert([
            ['cognito_username' => 0, 'email' => 'hai.le.connectiv+1@gmail.com',  'created_at' => now(), 'updated_at' => now()],
            ['cognito_username' => 0, 'email' => 'hai.le.connectiv+2@gmail.com',  'created_at' => now(), 'updated_at' => now()],
            ['cognito_username' => 0, 'email' => 'hai.le.connectiv+50@gmail.com',  'created_at' => now(), 'updated_at' => now()]
        ]);

        Schema::enableForeignKeyConstraints();
    }
}
