<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @Function: login
     * @Description: Global login function
     * @return string access_token
     */
    public function login()
    {
        $credentials = $this->parseCredentials();

        $response = $this->json('POST', 'api/login', $credentials);

        return json_decode($response->getContent(), true)['access_token'];
    }

    /**
     * @Function: parseCredentials
     * @Description: Get credentials follow
     * @return string[] email, password
     */
    public function parseCredentials()
    {

        $credentials = [
            'email' => 'hai.le.connectiv+50@gmail.com',
            'password' => '123456789aA',
        ];

        return $credentials;
    }
}
