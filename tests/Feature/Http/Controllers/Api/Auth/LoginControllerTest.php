<?php

namespace Tests\Feature\Http\Controllers\Api\Auth;

use Tests\TestCase;
//use Illuminate\Foundation\Testing\WithFaker;
//use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginControllerTest extends TestCase
{

    public function setUp() : void
    {
        parent::setUp();
        $this->artisan('db:seed', ['--class' => 'UsersTableSeeder']);
    }


    /**
     * @Function dataTestLoginInvalid
     *
     * @Description data for test login invalid case
     *
     * @return array
     */

    public function dataTestLoginInvalid()
    {
        return require_once('data/data_test_login_invalid.php');
    }

    /**
     * @Function testLoginInvalid
     *
     * @Description test login function invalid case
     *
     * @param $params
     * @param $expected
     *
     * @dataProvider dataTestLoginInvalid
     */
    public function testLoginInvalid($params, $expected)
    {
        $response = $this->json('POST', 'api/login', $params);
        $response->assertStatus($expected);
    }


    /**
     * @Function dataTestLoginNormal
     *
     * @Description data for test login normal case
     *
     * @return array
     */
    public function dataTestLoginNormal()
    {
        return require_once('data/data_test_login_normal.php');
    }

    /**
     * @Function testLoginNormal
     *
     * @Description test login function normal case
     *
     * @param $params
     * @param $expected
     *
     * @dataProvider dataTestLoginNormal
     */
    public function testLoginNormal($params, $expected)
    {
        $response = $this->json('POST', 'api/login', $params);
        $response->assertStatus($expected);
    }

//    public function testLogin()
//    {
//        // // Email Invalid
//        $response = $this->json('POST', 'api/login', [
//            'email' => 'hai.le.con nectiv+61@gmail.com',
//            'password' => '123456789aA',
//        ]);
//        $response->assertStatus(422);
//
//        // Email required
//        $response = $this->json('POST', 'api/login', [
//            'email' => '',
//            'password' => '123456789aA',
//        ]);
//        $response->assertStatus(422);
//
//        // Password required
//        $response = $this->json('POST', 'api/login', [
//            'email' => 'hai.le.connectiv+1@gmail.com',
//            'password' => '',
//        ]);
//        $response->assertStatus(422);
//
//        // Password too short
//        $response = $this->json('POST', 'api/login', [
//            'email' => 'hai.le.connectiv+1@gmail.com',
//            'password' => '1234',
//        ]);
//        $response->assertStatus(422);
//    }
}
