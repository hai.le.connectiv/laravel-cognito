<?php

namespace Tests\Feature\Http\Controllers\Api\Auth;

use App\Cognito\CognitoClient;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function setUp() : void
    {
        parent::setUp();
    }

    /**
     * @Function dataTestRegisterUserInvalid
     * @Description data for test register for user invalid case
     *
     * @return array
     */
    public function dataTestRegisterUserInvalid()
    {
        return require_once('data/data_test_user_register_invalid.php');
    }

    /**
     * @Function testRegisterUserInvalid
     * @Description test register user function invalid case
     *
     * @param $params
     * @param $expected
     * @dataProvider dataTestRegisterUserInvalid
     */
    public function testRegisterUserInvalid($params, $expected)
    {
        $response = $this->json('POST', 'api/register', $params);
        $response->assertStatus($expected);
    }

    /**
     * @Function dataTestRegisterUserNormal
     * @Description data for test register user normal case
     *
     * @return array
     */
    public function dataTestRegisterUserNormal()
    {
        return require_once('data/data_test_user_register_normal.php');
    }

    /**
     * @Function testRegisterUserNormal
     * @Description test register user function normal case
     *
     * @param $params
     * @param $expected
     * @dataProvider dataTestRegisterUserNormal
     */
    public function testRegisterUserNormal($params, $expected)
    {
        $response = $this->json('POST', 'api/register', $params);

        $response->assertStatus($expected);

//        $response->assertExactJson(["message" => "User registration successful."]);
    }

    /**
     * @Function dataTestConfirmRegisterNormal
     * @Description data for test update email normal case
     *
     * @return array
     */
    public function dataTestConfirmRegisterNormal()
    {
        return require_once('data/data_test_confirm_register_normal.php');
    }

    /**
     * @Function testUpdateNormal
     * @Description test update email function normal case
     *
     * @param $params
     * @dataProvider dataTestConfirmRegisterNormal
     */
    public function testConfirmRegisterNormal($params)
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $this->login()
        ])->json('POST', "api/user-register-confirmed", $params);
        $response->assertStatus(200);
    }
}
