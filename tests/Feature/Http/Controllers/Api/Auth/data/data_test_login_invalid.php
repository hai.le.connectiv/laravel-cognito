<?php

return [
    [
        [
            'email' => '', // Email required
            'password' => '12345678',
        ], 422
    ],
    [
        [
            'email' => 'hai.le.con nectiv+61@gmail.com', // Email Invalid
            'password' => '12345678',
        ], 422
    ],
    [
        [
            'email' => 'user.connectiv@gmail.com', // Password required
            'password' => '',
        ], 422
    ],
    [
        [
            'email' => 'user.connectiv@gmail.com', // Password too short
            'password' => '1234',
        ], 422
    ]
];
