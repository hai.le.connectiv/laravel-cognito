<?php

return [
    [
        [
            'email' => '',
            'password' => '12345678' // Email null
        ], 422
    ],
    [
        [
            'email' => 'email.connectiv+00@gmail.com',
            'password' => 'a_12345' // Password must more than 8 character
        ], 422
    ],
    [
        [
            'email' => 'email.connectiv+02@gmail.com',
            'password' => '' // Password null
        ], 422
    ]
];
