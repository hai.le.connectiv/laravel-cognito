<?php

namespace Tests\Feature\Http\Controllers\Api\Users;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * @Function dataTestShowInvalid
     * @Description data for test show detail user invalid case
     *
     * @return array
     */
    public function dataTestShowInvalid()
    {
        return [
            [500]
        ];
    }

    /**
     * @Function testShowInvalid
     * @Description test get show invalid case
     *
     * @param $expected
     * @dataProvider dataTestShowInvalid
     */
    public function testShowInvalid($expected)
    {
        $response = $this->json('GET', "api/user-info");

        $response->assertStatus($expected);
    }


    /**
     * @Function dataTestUpdateEmailNormal
     * @Description data for test update email normal case
     *
     * @return array
     */
    public function dataTestUpdateEmailNormal()
    {
        return require_once('data/data_test_update_email_normal.php');
    }

    /**
     * @Function testUpdateNormal
     * @Description test update email function normal case
     *
     * @param $params
     * @dataProvider dataTestUpdateEmailNormal
     */
    public function testUpdateEmailNormal($params)
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $this->login()
        ])->json('POST', "api/update-email-verification", $params);
        $response->assertStatus(200);
    }


    /**
     * @Function dataTestConfirmUpdateEmailNormal
     * @Description data for test update email normal case
     *
     * @return array
     */
    public function dataTestConfirmUpdateEmailNormal()
    {
        return require_once('data/data_test_confirm_update_email_normal.php');
    }

    /**
     * @Function testUpdateNormal
     * @Description test update email function normal case
     *
     * @param $params
     * @dataProvider dataTestUpdateEmailNormal
     */
    public function testConfirmUpdateEmailNormal($params)
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $this->login()
        ])->json('POST', "api/confirm-update-email-verification", $params);
        $response->assertStatus(200);
    }


}
